import {observer} from "mobx-react-lite";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import CircleIcon from '@mui/icons-material/Circle';
import CheckIcon from '@mui/icons-material/Check';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import {useStores} from "../state/context/Context";
export const MyEventTypeSelect = observer((props) =>
{

    const {eventStore} = useStores();

    const handleEventTypeOnChange = (value,id) => {
        const eventIndex = eventStore.eventList.findIndex(e => e.id === id);
        eventStore.setEvent('type',value, eventIndex);
    }

    return (
        <Select
            IconComponent={ArrowDropDownIcon}
            sx={{ height:'56px' , marginRight:'10px', width : props.width}}
            value={props.event.type}
            key = {props.key}
            onChange={ (e) => handleEventTypeOnChange(e.target.value,props.event.id)}>
            <MenuItem value={'default'}>
                <CircleIcon />
            </MenuItem>
            <MenuItem value={'check'}>
                <CheckIcon />
            </MenuItem>
            <MenuItem value={'success'}>
                <ThumbUpIcon />
            </MenuItem>
        </Select>
    );
})
