import {useStores} from "../state/context/Context";
import {Box, TextField} from "@mui/material";
import {observer} from "mobx-react-lite";
import {MyEventTypeSelect} from "./MyEventTypeSelect";
import {toJS} from "mobx";
import moment from "moment/moment";

export const MyScheduleList = observer((props) =>
{
    const {eventStore, calendarStore} = useStores();

    const handleTextBoxOnChange = (value,id) => {
        const eventIndex = eventStore.eventList.findIndex(e => e.id === id);
        eventStore.setEvent('title',value, eventIndex);
        localStorage.setItem('eventList', JSON.stringify(toJS((eventStore.eventList))));
    }

    const handleMyScheduleListEnterPress = (e) => {
        if(e.key === 'Enter' && e.target.value !== '')
        {
            const day = moment(calendarStore.currentDate).format('YYYY-MM-DD');
            const event = calendarStore.createEvent(e.target.value,day,day);
            eventStore.pushEventList(event);
            localStorage.setItem('eventList', JSON.stringify(toJS((eventStore.eventList))));
            e.target.value ='';
        }
        console.log(e.key);
    }

    const width = props.width -35;
    return (
        <>
            {(eventStore.eventList.filter( e => e.start == calendarStore.currentDate)).map( (e,i) => {
                return (
                    <Box key={calendarStore.currentDate+ i}>
                        <MyEventTypeSelect width={'80px'} event={e}/>
                        <TextField value = {e.title} sx={{  width : width - 100 , marginBottom : '10px'}} onChange={event => handleTextBoxOnChange(event.target.value,e.id)}></TextField>
                    </Box>
                )
            })}
            <TextField sx={{  width : width-10}} onKeyDown={handleMyScheduleListEnterPress} placeholder='등록하실려면 엔터를 눌러주세요'/>
        </>
    )
})
