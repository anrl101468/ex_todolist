import React, {useEffect, useState} from 'react';
import { Navigate } from 'react-big-calendar';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import moment from "moment";
import CircleIcon from '@mui/icons-material/Circle';
import CheckIcon from '@mui/icons-material/Check';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import {useStores} from "../state/context/Context";
import {observer} from "mobx-react-lite";
export const MyCustomCalendarToolbar = observer(({ label, onNavigate }) => {

    const {eventStore,calendarStore} = useStores();
    const [currentMonth,setCurrentMonth] = useState('');
    const date = moment(label,'MMMM YYYY');

    useEffect(() => {
        setCurrentMonth(moment(date).format('YYYY-MM'))
    }, []);

    const goToPrevMonth = () => {
        onNavigate(Navigate.PREVIOUS);
        setCurrentMonth(moment(date).add(-1, 'month').format('YYYY-MM'));
    }
    const goToNextMonth = () => {
        onNavigate(Navigate.NEXT);
        setCurrentMonth(moment(date).add(1, 'month').format('YYYY-MM'));
    };

    const customLabel = date.format('YYYY년 MM월');

    return (
        <div style={{display:'flex', justifyContent:'space-around'}}>
            <ArrowBackIcon onClick={goToPrevMonth}>Prev</ArrowBackIcon>
            <div style={{display: 'flex'}}>
                {customLabel}
                <div style={{display: 'flex', marginLeft: '20px'}}>
                    <div style={{display: 'flex', marginLeft: '10px'}}>
                        <CircleIcon/>
                        <span>{eventStore.getMatchedEventTypeCount('default',currentMonth)}</span>
                    </div>
                    <div style={{display: 'flex', marginLeft: '10px'}}>
                        <CheckIcon/>
                        <span>{eventStore.getMatchedEventTypeCount('check',currentMonth)}</span>
                    </div>
                    <div style={{display: 'flex', marginLeft: '10px'}}>
                        <ThumbUpIcon/>
                        <span>{eventStore.getMatchedEventTypeCount('success',currentMonth)}</span>
                    </div>
                </div>
            </div>
            <ArrowForwardIcon onClick={goToNextMonth}>Next</ArrowForwardIcon>
        </div>
    );
});
