import {Calendar, momentLocalizer, Views} from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import {observer} from "mobx-react-lite";
import {useStores} from "../state/context/Context";
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'
import {MyCustomCalendarToolbar} from "./MyCustomCalendarToolbar";
import {useEffect} from "react";

const localizer = momentLocalizer(moment);

const DnDCalendar = withDragAndDrop(Calendar);

export const MyCalendar = observer((props) => {

    const {eventStore, calendarStore} = useStores();

    return (
        <div className="myCustomHeight">
            <DnDCalendar
                localizer={localizer}
                components={{ toolbar : MyCustomCalendarToolbar}}
                defaultView={Views.MONTH}
                views={['month']}
                //events={props.events}
                events={eventStore.eventList}
                startAccessor="start"
                endAccessor="end"
                onSelectSlot={props.handleSelectSlot}
                //style={{ height: props.calendar.height , width: props.calendar.width}}
                //selectable={props.calendar.selectable}
                style={{height: calendarStore.calendar.height , width: calendarStore.calendar.width}}
                selectable={calendarStore.calendar.selectable}
                draggableAccessor={(event) => true}
                key={calendarStore.currentDate}
                onEventDrop={props.handleOnEventDrop}
                dayPropGetter={props.handleDayPropGetter}
            />
        </div>
        )
});
