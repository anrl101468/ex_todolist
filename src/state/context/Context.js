import React, { createContext, useContext } from "react";
import { RootStore } from "../store/RootStore";

const RootStoreContext = createContext();

export const RootStoreProvider = ({ children }) => {
    const rootStore = RootStore();

    return <RootStoreContext.Provider value={rootStore}>{children}</RootStoreContext.Provider>;
};

export const useStores = () => useContext(RootStoreContext);
