import {makeAutoObservable} from "mobx";
import moment from "moment";

export const CalendarStore = () => {
    const store = makeAutoObservable({
        calendar : {
            selectable : true,
            height : 500,
            width : 0,
        },
        currentDate : '',
        eventId : 0,
        createEvent : (title,start,end) =>  {
            const id = ++store.eventId;
            const type = 'default';
            localStorage.setItem('lastEventId',store.eventId);
            return {
                title,
                start,
                end,
                id,
                type,
            }
        },
        setCurrentDate : (date) =>{
          store.currentDate = date;
        },
        setCurrentMonth : (month) =>{
            store.currentMonth = month;
        },
        initCalendar : (height, width, selectable) => {
            store.calendar.selectable = selectable;
            store.calendar.height = height;
            store.calendar.width = width;
            store.currentDate = localStorage.getItem('currentDate');
            store.eventId = localStorage.getItem('lastEventId');
        },
    });
    return store;
};
