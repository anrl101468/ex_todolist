import {makeAutoObservable} from "mobx";
import {CalendarStore} from "./calendarStore/CalendarStore";
import {EventStore} from "./eventStore/EventStore";

export const RootStore = () => {
    const store = makeAutoObservable({
        calendarStore : CalendarStore(),
        eventStore : EventStore(),
    });

    return store;
};
