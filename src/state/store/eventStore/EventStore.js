import {action, makeAutoObservable, toJS} from "mobx";
import moment from "moment/moment";

export const EventStore = () => {
    const store = makeAutoObservable({
        eventList : [],
        eventType : [
            'default','check','success'
        ],
        initEventList : (eventList) => {
            if(eventList !== null){
                store.eventList = [ ...eventList];
            }
        },

        pushEventList :  (event) => {
            store.eventList = [...store.eventList, event];
        },

        setEvent : (column, value, index) => {
            store.eventList[index][column] = value;
            store.eventList = [...store.eventList];
            localStorage.setItem('eventList', JSON.stringify(toJS((store.eventList))));
        },

        moveEvent : ({event , start , end , isAllDay : droppedOnAllDaySlot = false}) => {
            const { allDay } = event;
            if(!allDay && droppedOnAllDaySlot) {
                event.allDay = true;
            }
            const existing = store.eventList.find((ev) => ev.id === event.id) ?? {};
            const filtered = store.eventList.filter((ev) => ev.id !== event.id);

            const day = moment(start).format('YYYY-MM-DD');
            start = day;
            end = day;
            store.eventList = [...filtered, { ...existing, start, end, allDay }];
            localStorage.setItem('eventList', JSON.stringify(toJS((store.eventList))));
        },

        getMatchedEventTypeCount: (type,month) => {
            const matchedMonthEvents = store.eventList.filter(event => event.start.includes(month));
            const matchedTypeEvents = matchedMonthEvents.filter(event => event.type === type);
            return matchedTypeEvents.length;
        },
    });

    return store;
};
