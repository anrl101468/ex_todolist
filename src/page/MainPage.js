import {MyCalendar} from "../components/MyCalendar";
import {useEffect, useState} from "react";
import {useStores} from "../state/context/Context";
import {observer} from "mobx-react-lite";
import moment from "moment";
import {Box} from "@mui/material";
import {MyScheduleList} from "../components/MyScheduleList";
import {toJS} from "mobx";

export const MainPage = observer(() => {

    const {calendarStore,eventStore} = useStores();
    const height = window.innerHeight -300;
    useEffect(() => {
        calendarStore.initCalendar(height , window.innerWidth - 500, true);
        eventStore.initEventList(JSON.parse(localStorage.getItem('eventList')));
    }, []);


    /*const handleMyCalendarSelectSlot = ({start}) => {
        const eventTitle = window.prompt("추가할 일정을 입력하세요");

        if(eventTitle !== '' && eventTitle !== null)
        {
            const day = moment(start).toDate();
            const event = calendarStore.createEvent(eventTitle,day,day);
            eventStore.pushEventList(event);
            console.log(eventStore.eventList);
        }
    }*/

    const handleMyCalendarSelectSlot = ({start}) => {
        calendarStore.setCurrentDate(moment(start).format('YYYY-MM-DD'));
        localStorage.setItem('currentDate',moment(start).format('YYYY-MM-DD'));
        localStorage.setItem('currentMonth',moment(start).format('YYYY-MM'));
    }

    const handleMyCalendarDayPropGetter = (date) => {
        if (moment(date).isSame(calendarStore.currentDate, 'day')) {
            return {
                style: {
                    backgroundColor: "darkseagreen",
                }
            };
        }
        return {};
    }



    return (
        <div style={{ display: 'flex'}}>
            <MyCalendar handleSelectSlot={handleMyCalendarSelectSlot}
                        width={400}
                        calendar={calendarStore.calendar}
                        events={eventStore.eventList}
                        handleOnEventDrop={eventStore.moveEvent}
                        handleDayPropGetter={handleMyCalendarDayPropGetter}
            />
            <Box component="section" sx={{ p: 2, border: '1px dashed grey' , width : 500, height : height}}>
                <MyScheduleList width ={500} height = {height} />
            </Box>
        </div>
    )
})
